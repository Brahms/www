---
title: "{{ replace .Name "-" " " | title }}"
location: Cyberspace
time: 13:37 Uhr
date: {{ .Date }} # Ersetz mich mit dem Talk Datum
draft: true
---
Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.

## Vortragende
[Max Mustermann](http://example.com/)

## Slides
[graphviz.pdf](/talks/example.pdf)

## Links
* [Example](http://example.com)