---
title: "Neuer Dienst: Übungszettelaggregator"
tags: ['service']
date: 2010-11-07T02:00:00+00:00
draft: false

---
Ein neuer Dienst ist online: [Zettel](http://zettel.spline.de) ist ein
Aggregatordienst für Übungszettel
