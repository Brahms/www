---
title: "Shellkurs WiSe 21/22"
date: 2021-10-20T00:55:54+02:00
draft: false
aliases:
  - /shellkurs
---

Im Anschluss zu unserer Installparty vom letzten Freitag veranstalten wir am 20. Oktober 2021 um 18 Uhr einen weiterführenden Shellkurs.  
  
Wir empfehlen eine Teilnahme für unter anderem Studienanfänger*innen, die noch Hilfe beim Einstieg benötigen.  
Die Veranstaltung wird als Workshop durchgeführt zum optionalem Live mitmachen. Benötigt wird dafür entweder ein Linux, welches zum Beispiel bei der Installparty erhältlich war oder ein [SSH Client](https://www.mi.fu-berlin.de/w/IT/ItServicesSSHAccess).  
  
Der Workshop wird Online durchgeführt über BigBlueButton und kann über diesen Link besucht werden: https://bbb.planet.fu-berlin.de/b/vin-xqa-uys-jgd .  
  
Folien (WIP) findet ihr [hier](https://spline-fu.github.io/shellkurs/)  