---
title: "End of SplineForge"
tags: ['service', 'dev', 'splineforge']
date: 2008-10-15T02:00:00+00:00
draft: false

---
The SplineForge service will soon be discontinued due to maintenance
problems and will be replaced by [dev.spline.de](https://dev.spline.de).  This
means that your account and projects will be gone if you do not react to this
or any of the following reminder emails.

We can migrate your projects to dev.spline.de, but ONLY the Subversion
repositories.  If you want to keep wikis/mailinglists/.. you will have to
back them up yourself.

Every project in SplineTrac will have a new wiki/mailinglist/..

If you want to migrate a project, please send an email to dev@spline.de
including your gforge username. also, you must have a dev.spline.de account,
otherwise we won't know where to migrate your project to!

Please note that as of now dev.spline.de should be regarded as being in
"testing" stage, because we just recently set it up and are still fixing bugs.
So if you encounter a bug, please send an email to dev@spline.de.

We apologize for any inconvenience and thank you for your understanding.

the dev.spline.de team
