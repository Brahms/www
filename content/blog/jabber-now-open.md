---
title: "Jabber frei zur Registierung"
tags: ['service', 'jabber']
date: 2011-10-27T02:00:00+00:00
draft: false

---
Unser [Jabber](http://jabber.spline.de)-Server ist jetzt testweise für alle
offen. Wir können IPv6, SSL und sind (mit dns srv) redundant.

---

Ab sofort wird der [Spline](http://www.spline.de) Jaber-Server für jeden
nutzbar sein. Das bedeutet, man kann sich mit einem
[Jabber-Client](http://xmpp.org/xmpp-software/clients/) seiner Wahl einen
Account für jabber.spline.de registrieren.
<ins>Es gibt ein <a href="{{ url_for('services/jabber/') }}">aktuelles Zertifikat</a>!
</ins><del>Falls ihr der Startcom SSL CA nicht traut, ist hier der Fingerprint des SSL-Zertifikats:</del>

<br>

<del>
<p>
<code>
0d:16:b6:9a:08:d1:52:13:1b:ff:f7:0a:c8:75:7f:93:58:fb:41:0a
</code>
</p>
</del>

<br>

Bei Schwierigkeiten oder Fragen könnt ihr euch per Mail an die
[Maintainer](jabber@spline.de) wenden oder im IRC-Channel #spline auf
*irc.freenode.net*  vorbei schauen.



