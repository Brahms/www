---
title: "Erstitreffen"
tags: ['erstsemestler', 'spline']
date: 2013-04-03T02:00:00+00:00
draft: false

---
Das neue Semester steht vor der Tür und auch Spline wünscht allen Studierenden
viel Erfolg! Für alle Interessierte gibt es am **16.04** ab **16:00** im
Splineraum ein Spline-Erstsemester-Treffen. Hier stellen wir Spline selbst vor
und möchten mit einer Linux-Install-Party anschließen. Desweiteren wird es
auch einen kleinen Workshop zur hardwarenahen Programmierung mit der
Arduino-Platform für Einsteiger geben. Kommt also vorbei!
