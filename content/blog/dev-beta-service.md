---
title: "Dev im Beta-Betrieb"
tags: ['service', 'dev']
date: 2008-09-20T02:00:00+00:00
draft: false

---
[dev.spline.de](http://dev.spline.de), seit kurzem im BETA-Betrieb,
hat jetzt für die nächsten 30 Tage ein gültiges SSl-Zertifikat.

Ob wir auf Dauer eins finanzieren wollen, wird gerade diskutiert.
Spenden werden wie immer gern angenommen. :-)

Meldet Euch wenn Ihr vom alten Entwicklungs-System (gforge) noch Daten braucht.

**UPDATE**: dev.spline.de hat jetzt ein richtiges Zertifikat.
