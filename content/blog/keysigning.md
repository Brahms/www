---
title: "Keysigning-Party"
tags: ['keysigning']
date: 2006-06-21T02:00:00+00:00
draft: false

---

## Termin

Montag, 3. Juli 2006, 17.30 Uhr
Informatik-Gebäude, Takustraße 9, Raum 5*

## Keysigning?
Eine Keysigning-Party ist ein lockeres Treffen von Nutzern und Nutzerinnen des PGP-Verschlüsselungsverfahrens. Die Teilnehmer der Party überprüfen ihre Identität und signieren gegenseitig ihre öffentlichen PGP-Schlüssel. Als Resultat einer solchen Party entsteht ein Web-of-Trust, also ein Netz von Nutzern, die gegenseitig die Beziehung zwischen PGP-Schlüssel und Identität bestätigen.

* Bild des Lokalen Web-of-Trust nach unserer letzten Keysigning-Party.

Keysigning findet überall auf der Welt statt und so ist es kein Wunder, dass viele kleine lokale Netze mit einem weltweiten Web-of-Trust verbunden sind. Mit der Größe und Vermaschung dieses Netzes steigt die Aussagekraft von PGP-Signaturen, z.B. durch das Verfolgen von Vertrauenspfaden.

* Vertrauenspfad durch das Web-of-Trust von Bogumil zu Richard Stallmann.

Wer also wünscht, dass andere seinen signierten E-mails vertrauen, der sollte an einer Keysigning-Party teilnehmen. Hier kann die Beziehung zwischen PGP-Schlüssel und Identität von vielen unabhängigen Personen dezentral bestätigt werden und das kostenlos.
Weitere und ausführlichere Informationen zu Keysigning-Partys befinden sich im deutschen und englischen HowTo von GnuPG, einer freien und sehr umfangreichen Implementierung von PGP.

## Teilnehmen
Zu unserer Keysigning-Party sind alle Interessierten herzlich eingeladen. Wer aktiv teilnehmen möchte, sollte allerdings die folgenden Punkte beachten:
* Falls noch nicht vorhanden, muss ein PGP-Schlüsselpaar (privater und öffentlicher Schlüssel) generiert werden. Behilflich sind dabei:
** Anleitung für gpg unter Windows (auch mit GUI)
** mit Seahorse oder GPA unter Linux
** Eine ausführliche deutsche und englische Anleitung zur Schlüsselerzeugung.
** Wir empfehlen bei der Erstellung des Schlüssels auf die Mailadresse (und folgenden Spam) zu verzichten.
* Der eigene, öffentliche PGP-Schlüssel muss vor der Party an bogus-gpg (at) spline.inf.fu-berlin.de gesendet werden, da nur so alle Schlüssel-Fingerprints für alle Teilnehmer rechtzeitig ausgedruckt werden können.
* Zur Party sind die folgenden Dinge mitzubringen:
** Personalausweiß oder Pass
** Ausdruck der eigenen Schlüssel-ID und des Schlüssel-Fingerprints
** Etwas zum Schreiben, z.B. Stift und Papier
Computer sollten falls möglich nicht eingesetzt oder mitgebracht werden, da es auf einer Keysigning-Party viele Möglichkeiten gibt, Böses zu tun, wie z.B. das Erspähen von Passwörtern oder das Einschleusen von "Hintertüren".
Desweiteren dürfen neben diesen lästigen Punkten natürlich nicht eine gute Laune und der Spaß zu Hause vergessen werden.

## Ablauf
Die Keysigning-Party wird in drei Phasen durchgeführt. Nach einer kurzen und mathematikfreien Einführung zu den Grundlagen von Kryptographie und PGP geht es mit der ersten Phase los.

* Die Teilnehmer erhalten die Schlüssel-ID und -Fingerprints aller anderen Teilnehmer als Ausdruck. Es wird überprüft, ob alle Ausdrucke gleich sind.
* In der zweiten Phase werden alle Teilnehmer der Reihe nach allen anderen ihren Personalausweis zeigen, so dass alle Identitäten bestätigt werden können. Stimmt die Identität überein, markieren die Teilnehmer die Schlüssel-ID, den entsprechenden Fingerprint und Namen als korrekt.

Nachdem alles überprüft und markiert worden ist, ist der offizielle Teil beendet und die Party kann auf verschiedene Weise ... fortgesetzt werden. Die entscheidende dritte Phase, das eigentliche Keysigning, wird von jedem Teilnehmer zu Hause und alleine durchgeführt.

* Jeder Teilnehmer importiert und signiert mit seinem privaten PGP-Schlüssel alle Schlüssel, die auf der Party zweifach markiert wurden. Anschließend sendet er die signierten Schlüssel an einen öffentlichen Schlüsselserver oder per E-mail an den Besitzer zurück. Eine deutsche und englische Anleitung zum Signieren von PGP-Schlüsseln ist ebenfalls Teil des GnuPG-HowTos.

**UPDATE:** entstandene Web-of-Trust (6.6.2006)

![Web-of-Trust](/images/blog/web-of-trust-july-2006.png)
