---
title: "ejabberd + accounts.spline.de"
tags: ['accounts', 'jabber']
date: 2013-10-21T02:00:00+00:00
draft: false

---

Es ist endlich soweit.
Der jabber-Server funktioniert jetzt auch mit

[https://accounts.spline.de](https://accounts.spline.de)

Wir benutzen jetzt:

    {auth_method, [ldap, internal]}.

Das heisst, Menschen können sich sowohl mit dem alten Passwort, als auch mit
dem accounts-Passwort anmelden.  Intern gibt es dann tatsächlich zwei Einträge
in der auth-Tabelle, aber die Roster etc sollten davon nicht betroffen sein.
Solltet ihr doch Probleme haben, bitte meldet mir das sofort.

Ausserdem habe ich `mod_register` durch `mod_register_refer` ausgetauscht. Der Code dazu liegt hier:


[http://git.spline.de/ejabberd/mod_register_refer/](http://git.spline.de/ejabberd/mod_register_refer/)


Da ich nicht durch den ganzen ejabberd-Code bin, kann da sicherlich was schief
gelaufen sein und ich wäre euch sehr verbunden, wenn ihr mal das
Neu-Registrieren mit euren Clients (libpurple, gajim, psi hab ich getestet)
probiert und mir schreibt, wenn irgendetwas falsch läuft.


