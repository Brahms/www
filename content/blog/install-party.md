---
title: "Installparty 25. Oktober 2012"
tags: ['installparty', 'spline']
date: 2012-10-05T02:00:00+00:00
draft: false

---

Wer Hilfe bei der Linuxinstallation auf seinem Rechner/Notebook
wünscht oder irgendwelche Probleme mit seiner bestehenden Installation
hat, ist eingeladen sich (mit seinem Gerät) am

  **Donnerstag den 25.10. um 16 Uhr im Raum K60**

einzufinden. Wer weitere Fragen rund um das Thema "Freie Software" hat,
ist ebenfalls herzlich Willkommen.

Kekse und Kaffee werden vorhanden sein.

Ihr solltet folgendes mitbringen:

* Computer
* Netzteil für Laptop
* Festplatte mit fertigen Backups
* mehr Kekse
* eine ungefähre Vorstellung was ihr haben wollt (Dualboot oder nur Linux)

