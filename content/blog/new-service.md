---
title: "Klausurenarchiv"
tags: ['service', 'spline', 'klausurenarchiv', 'klausuren']
date: 2012-07-12T02:00:00+00:00
draft: false

---
Nach kleiner Hacksession haben wir wieder mal einen neuen Dienst.
Diesmal wahrscheinlich ein Segen für viele Studierende: **ein Klausurenarchiv**.
Zur Zeit sind noch nich all zu viele Klausuren hochgeladen, aber mit euer
Mithilfe wird sich das hoffentlich schnell ändern.

Implementiert wurde das Ganze in Python mit Hilfe des Webframeworks Flask, sowie
pygit2. Als Storage-Backend wird ein normales Git-Repository verwendet. Dies hat
den netten Nebeneffekt dass statt alle Klausuren einzeln herunterzuladen, könnt
ihr die kompletten Daten auch mittels einem einfachen *git clone* bekommen.

Zu finden ist der neue Dienst unter
[klausuren.spline.de](http://klausuren.spline.de)
