---
title: "Install-Party am 16. Oktober 2013"
tags: ['spline', 'lip', 'installparty']
date: 2013-10-13T02:00:00+00:00
draft: false

---

Diesen **Mittwoch am 16.10** findet **ab 13 Uhr** im **Raum SR006** in
der **Takustaße 9** die Linuxinstallparty statt. Freundliche
Unterstützung erhalten wir diesmal von der
[Belug](http://www.belug.de/).

Frage? Antworten!
-----------------

*Was sollten TeilnehmerInnen **vorher** machen?*

- Backups machen
- Backups machen
- überlegen ob sie Windows behalten wollen
- Backups machen
- bei Dualinstallationen das Windows defragmentieren (optional, spart Zeit)


*Was sollten TeilnehmerInnen mitbringen?*

- Netzteil (für den Laptop)
- externe Festplatte mit Backups
- ausreichend Zeit (2 Stunden wird es sicher dauern)

*Was kann installiert werden?*

Eigentlich fast alles was freie Software ist. Es geht natürlich vorallem um
das Betriebsystem und da liegt unser Schwerpunkt auf Linux. Wir haben auch
Erfahrung mit Free- und OpenBSD.

*Auf was für Hardware wird installiert?*

Alles an "normaler" Laptophardware (Intel oder AMD) ist kein Problem. Es wird
manchmal problematisch, wenn ihr ein sehr neues Model mit EFI habt. Aber das
klären wir vor Ort.

Wenn ihr euren Standrechner von zu Hause mitbringen wollte, schreibt uns
vorher. Wir können Monitore organisieren.

Bei Smartbooks, Smartphones und Tablets wird es meistens etwas schwieriger,
daher haben wir dafür eine seperate Installparty am 24.10 um 16:00 Uhr.


*Was ist die einzig wahre Distribution?*

Da hat unsere Gruppe keine eindeutige Meinung, ja nach deinem Vorwissen und
unserer Laune installieren wir Ubunt, Xubunt, Lubunt, Debian, Fedora, ArchLinux,
Gentoo, oder was ganz anderes :)



