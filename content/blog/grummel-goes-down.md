---
title: "Grummel goes down!"
tags: ['service', 'splineforge', 'grummel']
date: 2008-09-10T02:00:00+00:00
draft: false

---
Nach Störungen im DMZ-Netz musste Grummel vom Netz getrennt werden,
da dieser Netzstörungen auslöste. Sobald sich trotz VL-freier Zeit passende
"Grummel"-Experten für eine Untersuchung finden und die Ursache beseitigen
können, kann Grummel samt spline-Forge wieder ans Netz gehen.
