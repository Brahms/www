---
title: mapsforge - OpenStreetMap Navigation und Karten auf Android
location: Hörsaal der Informatik
time: 18:00 Uhr
date: 2010-07-05T12:00:00+01:00
---
Angefangen mit einem kurzen Überblick über die Programmierung unter Android und
die Besonderheiten der Plattform, wird der Fokus dieses Vortrags auf der
Entwicklung der Kartenkomponente für Android und den damit verbundenen
Problemstellungen (zum Beispiel: Kartenrendering auf mobilen Endgeräten)
liegen. Der Vortrag wird abgerundet durch einen kurzen Überblick zur Umsetzung
von Routingalgorithmen auf mobilen Geräten.

## Vortragende
Jürgen Broß, Thilo Mühlberg

## Slides
[mapsforge.pdf](/talks/mapsforge.pdf)

## Links
* [mapsforge](http://mapsforge.org/)
* [OpenStreetMap Navigation Projekt](http://inf.fu-berlin.de/en/groups/ag-db/projects/currentprojects/OSM/index.html)
* [OpenStreetMap](http://inf.fu-berlin.de/en/groups/ag-db/projects/currentprojects/OSM/index.html)
