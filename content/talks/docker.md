---
title: Docker
location: SR006 (Takustrasse 9)
time: 18:00 Uhr
date: 2018-06-12T12:00:00+01:00
---
Docker ist eine Containervirtualisierungs-Software, die sehr weit verbreitet ist. Viele Firmen und Unternehmen benutzen diese.
Es ist ein Open-Source Projekt und es gibt zwei Versionen: eine kostenpflichtige Enterprise-Edition und die freie Community-Edition.

In diesem Talk werden wir lernen, wie man Docker benutzt und eigene Images und Container baut sowie startet. 
Wir gehen am Anfang auch auf die Eigenschaften ein.

## Vortragende
[Alexander Gordon](mailto:gordon@spline.de)

## Slides
[docker.pdf](/talks/docker.pdf)