---
title: GnuPG
location: Hörsaal der Informatik
time: 18:00 Uhr
date: 2012-11-27T12:00:00+01:00
---
Dario Brandes wird eine praxisorientierte Einführung in die
Emailverschlüsselung mit GPG geben. Der GNU Privacy Guard ist eine freie
Implementierung des OpenPGP Protokolls. Damit können Emails zwischen Sender und
Empfänger vertraulich ausgetauscht und ihre Authentizität mittels Signaturen
überprüft werden. 

GnuPG setzt zur Überprüfung der Schlüsselauthentizität ein "Web of Trust".
Passend dazu wird am Dienstag den 4.12.2012 um 18 Uhr st bei spline eine
Keysigning-Party veranstaltet. Zu dieser Veranstaltung wird es sowohl auf dem
Vortrag als auch auf der Mailingliste noch weitere Informationen geben.

## Vortragende
[Dario Brandes](mailto:dalino(supersafespamschutz)spline(hiereinpunkt)de)

## Links
* [GNU Privacy Guard](http://www.gnupg.org/)