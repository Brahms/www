---
title: Bitcoin
location: Hörsaal der Informatik
time: 18:00 Uhr
date: 2011-11-23T12:00:00+01:00
---
Jeffrey Paul is going to give an introduction into Bitcoin. Bitcoin is a
digital Peer-to-Peer currency with no central authority which enables instant
payments to anyone, anywhere in the world. Jeffrey Paul will describe how
Bitcoin makes use of modern cryptography and how the decentralized algorithm
behind Bitcoin works. Apart from that he will discuss some of the consequences
the new currency could have on the society and the current economy.

Jeffrey Paul is a American hacker and entrepreneur currently based in Berlin.
Founder of datavibe.net (1999) and EEQJ (2009), he writes, presents, and
consults on a wide range of topics including civil liberties and practical
applications of networking, cryptography, and security systems.

## Vortragende
Jeffrey Paul ([@sneakatdatavibe](https://twitter.com/sneakatdatavibe))

## Links
* [Bitcoin](http://bitcoin.org/)
* [Satoshi Nakamoto](https://en.bitcoin.it/wiki/Satoshi_Nakamoto)
* [Bitcoin: A Peer-to-Peer Electronic Cash System](http://bitcoin.org/bitcoin.pdf)
