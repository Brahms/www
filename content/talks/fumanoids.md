---
title: FUmanoids
location: Hörsaal der Informatik
time: 18:00 Uhr
date: 2010-05-17T12:00:00+01:00
---
FUmanoids heißt das Team autonom fussballspielender, humanoider Roboter der FU
Berlin. Das Projekt wurde 2006 in der AG Künstliche Intelligenz als
Nachfolgeprojekt der FU Fighters gestartet; das Team konnte seitdem mehrere
Erfolge in der KidSize League des RoboCup erzielen, unter anderem wurde der 2.
Platz beim RoboCup 2009 in Graz erreicht und als jüngster Erfolg der 1. Platz
bei den IranOpen 2010 in Tehran gefeiert. Wie die Arbeit mit und an den
Robotern aussieht, welche Aufgaben es zu lösen gilt und wie der Roboter laufen,
sehen und agieren lernt, so dass er tatsächlich im Team Fussball spielen kann,
wird euch in diesem Vortrag gezeigt.

## Vortragende
Naja von Schmude, Stefan Otte

## Slides
[fumanoids.pdf](/talks/fumanoids.pdf)

## Links
* [FUmanoids](http://fumanoids.de/)
* [Twitter: FUmanoids](http://twitter.com/fumanoids)
* [Mitmachen!](http://fumanoids.de/join-us)
* [WP: Roboterfußball](http://de.wikipedia.org/wiki/Roboterfu%C3%9Fball)
* [RoboCup](http://www.robocup.org//)
* [XABSL](http://www.robocup.informatik.tu-darmstadt.de/xabsl/)
* [WP: Maschinelles Sehen](http://de.wikipedia.org/wiki/Maschinelles_Sehen)
* [WP: YUV Farbmodell](http://de.wikipedia.org/wiki/YUV)
* [WP: Hough-Transformatio](http://de.wikipedia.org/wiki/Hough-Transformation)
* [WP: Canny-Algorithmus](http://de.wikipedia.org/wiki/Canny-Algorithmus)
* [WP: Sobel-Operator](http://de.wikipedia.org/wiki/Sobel-Operator)
