---
title: Free your android!
location: Hörsaal der Informatik
time: 18:00 Uhr
date: 2012-12-11T12:00:00+01:00
---
Erik Albers von der Free Software Foundation Europe wird die Kampagne "Free
Your Android" vorstellen. 

Android ist ein weitgehend freies Betriebssystem für Smartphones. Leider sind
Treiber für die meisten Geräte sowie die meisten Programme, welche im Google
Play Store angeboten werden, nicht frei. Sie arbeiten immer wieder gegen die
Interessen der Nutzer, spionieren sie aus und können manchmal nicht einmal
entfernt werden. 

Die Kampagne "Free Your Android" soll dabei helfen, die Kontrolle über
Android-Geräte zurück zu gewinnen. Sie sammelt Informationen über einen
möglichst freien Android-Betrieb und will die Erkenntnisse in diesem Bereich
koordinieren.

## Vortragende
[Erik Albers](http://fsfe.org/about/albers/albers.html)

## Links
* [Free Your Android](http://fsfe.org/campaigns/android/android.html)
* [WP: Android (Betriebsystem)](http://de.wikipedia.org/wiki/Android_(Betriebssystem))
