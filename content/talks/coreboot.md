---
title: coreboot
location: SR005 (Takustr. 9)
time: 18:00 Uhr
date: 2013-10-29T12:00:00+01:00
---
Das BIOS kennen wir alle als freundliche blaue Textoberfläche, die wir meist
nur dann benutzen, wenn mal wieder von CD oder Netzwerk gestartet werden muss.
Trotzdem würde ohne es kein Computer laufen, da es die Hardwareintialsierung
übernimmt und den Bootcode lädt. In den letzen Jahren macht diese sonst eher
unaufällige Software viel von sich reden. So kommt mit UEFI eine neue
Schnittstelle, die den Fluch der Codesignierung durch Microsoft mit sich führt.
Außerdem versucht sich Schadsoftware im hardwarenächsten Teil unseres Computers
breitzumachen. Grund genug, um über eine freie Alternative nachzudenken.

Das Coreboot Projekt entwickelt ein gleichnamiges freies BIOS unter der GPL
Lizenz. Es bootet schneller als herkömmliche BIOS Software und ist deutlich
flexibler in der Anwendung, durch Trennung der Initialsierung und des Laden des
Bootcodes.

Vorgestellt wird das Projekt von Peter Stuge, einem der Mitentwickler von
coreboot. Er hat unter anderem auch schon auf der FOSDEM 2010 und dem 25C3
Vorträge über coreboot gehalten und wird ausführlich über Neuerungen berichten.
Im Anschluss habt ihr die Möglichkeit, ihn mit euren Fragen zum Projekt zu
löchern.

## Vortragende
[Peter Stuge](http://www.coreboot.org/User:Stuge)

## Links
* [Coreboot Projekt](http://www.coreboot.org/)