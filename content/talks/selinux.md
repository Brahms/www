---
title: SELinux
location: Hörsaal der Informatik
time: 18:00 Uhr
date: 2013-07-09T12:00:00+01:00
---
> Properly implemented strong crypto systems are one of the few things that
> you can rely on. Unfortunately, endpoint security is so terrifically weak that
> NSA can frequently find ways around it.
>
> <p style="text-align:right">(Edward Snowden)</p>

Wenn wir NutzerInnendaten schützen wollen, können wir uns nicht alleine auf
Verschlüsselung verlassen, denn irgendwo werden die Nachrichten oder Metadaten
wieder im Klartext gelesen und gespeichert. Zur Härtung von Servern kann
Security Enhanced Linux (SELinux) eingesetzt werden, um Einbrüche zu verhindern
oder den Schaden durch Einbrüche zu verringern.

Nach einer kurzen Einführung in SELinux wird am Beispiel u.a. des Mailservers
postfix gezeigt, wie SELinux eine im Vorhinein definierte policy durchsetzt,
wie eine SELinux policy aufgebaut ist, wie man eine bestehende policy an eigene
Zwecke anpasst und wie man eine policy für ganz neu schreibt. Auch wenn der
Fokus des Talks auf SELinux auf Servern liegt, wird es auch noch einige
Informationen zu SELinux auf Workstations und Desktops geben.

## Vortragende
[Mika Pflüger](mailto:mikapfl.dasistspomshutz@spline.de)

## Slides
[selinux.pdf](/talks/selinux.pdf)

## Links
* [http://wiki.debian.org/SELinux](http://wiki.debian.org/SELinux)