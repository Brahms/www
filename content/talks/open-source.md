---
title: Wie und warum man sich als Open Source Entwickler/in engagieren sollte
location: Hörsaal der Informatik
time: 18:00 Uhr
date: 2010-12-13T12:00:00+01:00
---
Viele Studenten könnten gute Programmierer sein, aber lassen Ihr Talent
ungenutzt oder stecken es in private (kleinst) Projekte und One-Man-Vaporware.
Würden sie sich hingegen in bestehenden Open Source Projekten engagieren, würde
ihr Lieblingsprojekt nicht nur ein Stück besser werden, sondern sie selbst
gewännen dabei eine Menge. Was ein paar dieser Vorteile sind, und wie man
überhaupt damit anfängt sich in einem großen Projekt zu beteiligen, möchte ich
am Beispiel von [KDE](http://kde.org/) zeigen und hoffe, dass dadurch ein paar
mehr von euch aktiv in einem FOSS Projekt mitarbeiten werden.

## Vortragende
[Milian Wolf](http://milianw.de)

## Slides
[kde-dev.pdf](/talks/kde-dev.pdf)

## Links
* [KDevelop](http://kdevelop.org/)
* [KDE e.V.](http://ev.kde.org/)
* [Desktop Summit](http://desktopsummit.org/)
* [Google Summer of Code](https://code.google.com/soc/)