---
title: Wenn Quellcode Laufen lernt - Einstieg in die Hardwareprogrammierung
location: Hörsaal der Informatik
time: 18:00 Uhr
date: 2010-06-07T12:00:00+01:00
---
Am Beispiel von 8-bit RISC Prozessoren wird euch dieser Vortrag den Einstieg in
die Welt der Mikroprozessoren und die damit verbunden Möglichkeiten aufzeigen.
Begonnen mit dem obligatorischen Hello World über die benötigte Grundaustattung
an Wissen und Werkzeug, Bezugsquellen, Fallstricke, C und uC, Links etc. bis
hin zu Animatronics (Animatronics sind die Roboter, die beim Film ferngesteuert
als Monster am Set rumhüpfen) soll euch dieser Talk einen leichten Einstieg und
Ausblick in die Welt des Hardware Hacking geben und motivieren, selbst kreativ
zu werden.

## Vortragende
Friedrich Wessel, Alexander Surma

## Slides
[hardwarehacking.pdf](/talks/hardwarehacking.pdf)
