---
title: Protocol Buffers - Google's data interchange format
location: Hörsaal der Informatik
time: 18:00 Uhr
date: 2011-01-12T12:00:00+01:00
---
[Protocol Buffers](http://code.google.com/p/protobuf) ist eine von Google
entwickelte Methode zur Serialisierung von Daten in ein effizientes
Binärformat. Dieser Vortrag gibt einen Einstieg in die Verwendung von protobuf
und Einblick in Aufbau und Funktionsweise.

## Vortragende
[Daniel Seifert](http://www.inf.fu-berlin.de/groups/ag-ki/members/Seifert.html)

## Slides
[protobuf.pdf](/talks/protobuf.pdf)

## Links
* [Protobuf](http://code.google.com/p/protobuf)
* [Thrift](http://incubator.apache.org/thrift/)