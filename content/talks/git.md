---
title: Eine Einführung in Git
location: Hörsaal der Informatik
time: 18:00 Uhr
date: 2010-05-03T12:00:00+01:00
---
Git ist ein mächtiges Tool zur verteilten Versionsverwaltung von Dokumenten und
entwickelt sich zunehmend zum Platzhirsch und Subversion-Konkurrenten. Diese
Einführung soll euch einen Überblick und Einstieg in die Funktionsweise und
Nutzung von Git bieten.

## Vortragende
Alexander Sulfrian

## Slides
[git-scm.pdf](/talks/git-scm.pdf)

## Links
* [Git](http://git-scm.com/)
* [WP: Git](http://de.wikipedia.org/wiki/Git)
* [Git Community Book](http://book.git-scm.com/)
* [Git User's Manual](http://www.kernel.org/pub/software/scm/git/docs/user-manual.html)
* [CRE: Verteilte Versionskontrollsysteme](http://chaosradio.ccc.de/cre130.html)
* [Tech Talk: Linus Torvalds on git](http://www.youtube.com/watch?v=4XpnKHJAok8)
* [GitSvnComparison](https://git.wiki.kernel.org/index.php/GitSvnComparison)
