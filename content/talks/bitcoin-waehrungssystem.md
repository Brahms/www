---
title: Bitcoin als Währungssystem
location: Hörsaal der Informatik
time: 18:00 Uhr
date: 2013-05-25T12:00:00+01:00
---
Spätestens seit Frühjahr 2013 hat jeder Informatiker von Bitcoins gehört. Diese
neue Art von digitaler Währung stellt den Versuch dar, ohne Zentralbank
auszukommen, da Geld in einem peer-to-peer-Netz generiert wird. Die Funktion
des Geldes als Wertzeichen ist hier bis auf die Spitze getrieben worden, da
Bitcoins nur Ketten von Bits sind. 

Geld erfüllt aber auch andere Funktionen: Es kann als Zirkulationsmittel, als
Wertmaßstab und als Wertanlage dienen. Während Varianten von digitalen
Währungen, wie Digicash, ein zentrales Bankensystem voraussetzen und alle
Geldfunktionen erfüllen, sind Bitcoins als Wertmaßstab und als Wertanlage
ungeeignet. 

Im Vortrag werden diese neue digitalen Währungen dem zentralen Bankensystem
gegenübergestellt. Wir werden sehen, was prominente Ökonomen von Friedrich von
Hayek, über Piero Sraffa und bis zu Paul Krugmann über die Frage der
Privatisierung der Geldschöpfung zu sagen haben. Auf die Bitcoins kommen wir
dann mit diesen Erkenntnissen zurück.

## Vortragende
[Raúl Rojas](http://www.inf.fu-berlin.de/~rojas/)

## Links
* [Bitcoin](http://bitcoin.org/)
* [Satoshi Nakamoto](https://en.bitcoin.it/wiki/Satoshi_Nakamoto)
* [Bitcoin: A Peer-to-Peer Electronic Cash System](http://bitcoin.org/bitcoin.pdf)
