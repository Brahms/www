---
title: Node.js
location: Hörsaal der Informatik
time: 18:00 Uhr
date: 2011-01-26T12:00:00+01:00
---
Node.js ist eine serverseitige Evented I/O Javascript Runtime. Dadurch eignet sie sich, im Gegensatz zu Apache + PHP, für "Echtzeit-Webanwendungen" mit vielen parallelen Verbindungen (z.B. Etherpad, Chatserver, Newsticker, Proxy, Gameserver...). Der Vortrag gibt eine Einführung in die eventbasierte, threadfreie Serverprogrammierung mit Node.js.

## Vortragende
Lucas Jacob

## Links
* [Node.js](http://nodejs.org/)
* [WP: Node.js](https://secure.wikimedia.org/wikipedia/en/wiki/Node.js)
* [scalable-networking.pdf](http://bulk.fefe.de/scalable-networking.pdf)
* [Node.js Modules](https://github.com/ry/node/wiki/modules)
