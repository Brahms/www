---
title: Splinux
logo_url: /images/services/splinux.png
description: die Linux-Spline-User-Group
---
## Spline Linux User-Group

Die Spline Linux User-Group ist eine offene Liste für folgende Themen:

* Fragen zu Linux?
* öffentliche Spline-Themen
* ...

Du kannst dich [hier](http://lists.spline.de/mailman/listinfo/splinux) für die
Liste eintragen!