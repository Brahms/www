---
title: Chat
logo_url: /images/services/chat.png
description: Basierend auf Mattermost, welches eine Open Source, selbst gehostete Slack-alternative ist
link: https://chat.spline.de
---