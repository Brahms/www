---
title: Linux Install Party
logo_url: /images/services/lip.png
aliases:
  - /installparty
description: Du wolltest schon immer mal Linux ausprobieren? Dann ist das deine Chance!
---
## Linux Install Party

### Wann und Wo?
* Donnerstag, 12. April 2022 14:00 - 16:00
* Ort: SR006 im Institut für Informatik, Takustraße 9

### Hinweise zur Linux-Install-Party (LIP)

* [Handout Linux only](/handouts/SoSe22/Linux_Installparty_2022.pdf)

#### Problemen bei den wir euch helfen wollen
* Wie installiere ich Linux auf meinem Laptop oder PC?
* Wie verwende ich meine Netzwerkkarte/Soundkarte/Supertolles Gerät unter Linux
* Warum sollte ich freie Software auf meinem Rechner installieren und welche?
* (Anmerkung: soweit möglich unterstützen wir euch gerne auch bei anderen freien Betriebssystemen)
* Welche Software steht mir zur Verfügung, um XY zu machen?

#### Dinge die ihr beachten müsst
* Macht vorher eine Datensicherung von allen wichtigen Daten!
* Die Linux-Installation ist heutzutage sehr unproblematisch aber denkbar ist es schon noch, dass irgendetwas richtig schief läuft, wenn Murphys Gesetz zuschlägt.
* Die Datensicherung sollte auf eine externe Festplatte oder einem USB stick gemacht werden, die unabhängig vom Computer ist!
* Abgesehen davon ist es grundsätzlich immer eine gute Idee, regelmäßig Datensicherungen zu machen, weil ein Computer wie jede Maschine theoretisch zu jeder blöden Gelegenheit versagen kann.
* Aber genug der Schwarzmalerei! Wir freuen uns auf euer Kommen und wenn ihr zu Hause eine Datensicherung rumliegen habt, kann euch eigentlich nichts passieren.
