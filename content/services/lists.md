---
title: Mailinglisten
logo_url: /images/services/mailman.png
description: freier Mailinglistendienst für FU-Angehörige (Mailman)
link: http://lists.spline.de
---