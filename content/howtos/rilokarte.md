---
title: Compaq / HP Remote Inside Lights Out (RiLo-Karte)
---
# Compaq / HP Remote Inside Lights Out (RiLO) in normalen PCs
* RiLO-Remote-Management-Karten gibt es relativ günstig bei Ebay
* theoretisch sind sie nur für Compaq/HP-Server gedacht, praktisch funktionieren sie aber in den meisten normalen PCs ganz prächtig.
## Fernwartung
* Die Verwartung erfolgt am einfachsten mittels Webinterface.
* Das Webinterface hat ein Java-Applet, das den gesamten Bildschirminhalt (inkl. grafischem Modus) anzeigt, und die Tastatur- und Maus-Eingaben an den PC zurückreicht.
* *Eventuell kommen die Tastendrücke nicht an, dann muss man erst einmal in das Applet klicken!*
## BIOS- und SCSI-Controller-Meldungen:
![](https://spline.de/images/howto/rilo/Screenshot-rilo1.png)
## Fehlermeldung des BIOS:
![](https://spline.de/images/howto/rilo/Screenshot-rilo2.png)
## Login auf der Konsole:
![](https://spline.de/images/howto/rilo/Screenshot-rilo3.png)
## Technik
* Die RILO-karte wird statt einer VGA-Karte eingebaut,
* sie besitzt
    * einen Monitor-Ausgang SUB-D (VGA analog),
    * ein Adapterkabel um Maus und Tastatur durchzschleifen,
    * einen 100Mbit Ethernet-Port,
    * einen Eingang für die separate Stromversorgung.
        * notwendig, damit die Karte weiter läuft wenn der Rechner aus ist.
* In einem Compaq/HP-Server kann die Karte (mit passenden Kabeln) den Server ein- und ausschalten.  
![](https://spline.de/images/howto/rilo/rilo-karte.jpg)
## Konfiguration
1. Beim Boot nach den Bios-Meldungen bei der Meldung der RiLO-Karte [F8] drücken.
2. Grundeinstellungen im Bios-Setuo der Rilo-Karte vornehmen (von DHCP auf statische IP, z.B.)
3. Details danach im Webinterface einstellen (``https://<IP-Adresse>``)  
![](https://spline.de/images/howto/rilo/rilo-bios-einstellungen-netzwerk.jpg)
## Ein- und Ausschalten von normalen PCs
* mit selbst gebasteltem Kabel
* klappt bei uns ganz gut. (Fotos/Anleitung folgen)
    * notes to ourselves:
        * karte scheint potentialfreien Kontakt (relais) zu haben
        * darüber lässt sich der Power-State schalten
            * entweder parallel zum Power-Taster (''Niedervolt, niemals 230V und Karte verbinden!'')
        * oder über *Power-On* (grün) und *Masse* (schwarz) am [ATX-Anschluss](http://de.wikipedia.org/wiki/ATX-Format#Pinbelegung) des Mainboards (ungetestet)

* später hier mehr, siehe erstmal:
    * [http://www.paul.sladen.org/lights-out/riloe.html](http://www.paul.sladen.org/lights-out/riloe.html)
